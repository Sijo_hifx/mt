Pod::Spec.new do |spec|
  spec.name             = 'MT'
  spec.version          = '0.1.0'
  spec.summary          = 'A sample of MT.'
  
  spec.description      = <<-DESC
  TODO: Nothing to say right now.
                       DESC

  spec.homepage         = 'https://Sijo_hifx@bitbucket.org/Sijo_hifx/mt'
  spec.license          = { :type => 'MIT', :file => 'LICENSE' }
  spec.author           = { 'Sijo' => 'sijo@hifx.co.in' }
  spec.source           = { :git => 'https://Sijo_hifx@bitbucket.org/Sijo_hifx/mt.git', :tag => spec.version.to_s }
  
  spec.ios.deployment_target = '9.0'
  spec.swift_version         = '4.2'
  spec.requires_arc          = true

  spec.source_files = 'MT','MT/**/*.swift'
  
  # spec.resource_bundles = {
  #   'MT' => ['MT/Assets/*.png']
  # }
  
  spec.frameworks = 'Foundation'
  spec.dependency 'Reachability'
end
